/** @format */

const hamburger = document.getElementById("hamburger");
const btnHumber = document.getElementById("hamburgerBtn");
const btnXmark = document.getElementById("xmark");
hamburger.addEventListener("click", (item) => {
  btnHumber.classList.toggle("hidden");
  btnXmark.classList.toggle("hidden");
});
var fadeTarget = document.getElementById("lazyload");
var fadeEffect = setInterval(function () {
  fadeTarget.classList.toggle("hidden");
  clearInterval(fadeEffect);
}, 500);

document.querySelectorAll(".translate-action").forEach((element) => {
  element.addEventListener("click", (event) => {
    var lg = event.target.innerText;
    changeLanguage(lg);
  });
});

const sw = document.getElementById("switch");
sw.addEventListener("click", () => {
  if (sw.checked == true) {
    changeLanguage("ID");
  } else {
    changeLanguage("EN");
  }
});

function changeLanguage(lg) {
  document.querySelectorAll(".menu-name").forEach((element) => {
    var textEn = element.getAttribute("data-language-en");
    var textIn = element.getAttribute("data-language-id");
    if (lg == "EN") {
      element.textContent = textEn;
      sw.checked = false;
    } else {
      element.textContent = textIn;
      sw.checked = true;
    }
    document.querySelectorAll(".translate-action").forEach((el) => {
      var btnT = el.getAttribute("data-language");
      if (lg == btnT) {
        el.classList.add("font-bold");
      } else {
        el.classList.remove("font-bold");
      }
    });
  });
}
// document.getElementById("target").addEventListener('onLoad', fadeOutEffect);

//custom select
var x, i, j, l, ll, selElmnt, a, b, c;
/*look for any elements with the class "custom-select":*/
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /*for each element, create a new DIV that will act as the selected item:*/
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");

  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /*for each element, create a new DIV that will contain the option list:*/
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 0; j < ll; j++) {
    /*for each option in the original select element,
    create a new DIV that will act as an option item:*/

    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    if (j === 0) {
      c.setAttribute("class", "same-as-selected");
    }
    c.addEventListener("click", function (e) {
      /*when an item is clicked, update the original select box,
        and the selected item:*/
      var y, i, k, s, h, sl, yl;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML;
          y = this.parentNode.getElementsByClassName("same-as-selected");
          yl = y.length;
          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }
          this.setAttribute("class", "same-as-selected");
          break;
        }
      }
      h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function (e) {
    /*when the select box is clicked, close any other select boxes,
      and open/close the current select box:*/
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}
function closeAllSelect(elmnt) {
  /*a function that will close all select boxes in the document,
  except the current select box:*/
  var x,
    y,
    i,
    xl,
    yl,
    arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i);
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}
/*if the user clicks anywhere outside the select box,
then close all select boxes:*/
document.addEventListener("click", closeAllSelect);

const appMS = document.getElementById("appsMS");
const appIOS = document.getElementById("appsIOS");
const appAndroid = document.getElementById("appsAndroid");

if (navigator.userAgent.match(/(iPad|iPhone|iPod|Macintosh|MacIntel)/g)) {
  appIOS.classList.remove("hidden");
} else if (navigator.userAgent.match(/Android/g)) {
  appAndroid.classList.remove("hidden");
} else {
  appMS.classList.remove("hidden");
}
var menuMini = document.getElementById("menu-mini");
var navAction = document.getElementById("nav-action");
menuMini.addEventListener("click", function (e) {
  navAction.classList.toggle("active");
});

// setAttribute
document.addEventListener("contextmenu", function (e) {
  e.preventDefault();
});

// data-te-input-state-active

document.querySelectorAll(".custom-input").forEach((element) => {
  element.addEventListener("change", (event) => {
    if (event.target.value) {
      event.target.setAttribute("data-te-input-state-active", "");
    } else {
      event.target.removeAttribute("data-te-input-state-active", "");
    }
  });
});
